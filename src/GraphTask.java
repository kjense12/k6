import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() {

      Scanner scanner = new Scanner (System.in);

      System.out.println("Graph name: ");
      var graphName = scanner.nextLine();
      Graph g = new Graph (graphName);

      System.out.println("Enter number of vertices: ");
      var n = scanner.nextInt();

      System.out.println("Enter number of edges: ");
      var m = scanner.nextInt();

      // Create random directed graph
      g.createRandomSimpleGraph (n, m);

      // Set random lengths for every arc in graph.
      g.setRandomInfoForArc(5);

      System.out.println(g.findShortestCycleInvolvingVertex(g.first, g));

      // See how the setRandomInfoForArc has set the lengths of arcs.
      // Vertex v = g.first;
      // while (v != null) {
      //    Arc a = v.first;
      //    while (a != null){
      //       System.out.println(a.id + ":" + a.getArcInfo);
      //       a = a.next;
      //    }
      //    v = v.next;
      // }


      // See "table" which the dijkstra algorithm created.
      // for (Vertex vertex :
      //         g.ListOfVertexes) {
      //    System.out.println(vertex.getId() + ":" + vertex.getInfo() + ":" + vertex.previous + "\n");
      // }

      // Graph testGraph = new Graph("Graph_3");
      // Vertex a = new Vertex("a");
      // testGraph.first = a;
      // Vertex b = new Vertex("b");
      // a.next = b;
      // Vertex c = new Vertex("c");
      // b.next = c;
      // Vertex d = new Vertex("d");
      // c.next = d;
      // Vertex e = new Vertex("e");
      // d.next = e;
      //
      // Arc ab = new Arc("ab");
      // ab.setArcInfo(5);
      // Arc bd = new Arc("bd");
      // bd.setArcInfo(3);
      // Arc bc = new Arc("bc");
      // bc.setArcInfo(4);
      // Arc ce = new Arc("ce");
      // ce.setArcInfo(3);
      // Arc ed = new Arc("ed");
      // ed.setArcInfo(4);
      // Arc da = new Arc("da");
      // da.setArcInfo(5);
      //
      // a.first = ab;
      // ab.target = b;
      //
      // b.first = bc;
      // bc.next = bd;
      // bc.target = c;
      // bd.target = d;
      //
      // c.first = ce;
      // ce.target = e;
      //
      // e.first = ed;
      // ed.target = d;
      //
      // d.first = da;
      // da.target = a;
      //
      // testGraph.ListOfVertexes.add(a);
      // testGraph.ListOfVertexes.add(b);
      // testGraph.ListOfVertexes.add(c);
      // testGraph.ListOfVertexes.add(d);
      // testGraph.ListOfVertexes.add(e);
      //
      //
      // System.out.println(testGraph.findShortestCycleInvolvingVertex(a, testGraph));
      //
      //  for (Vertex vertex :
      //          testGraph.ListOfVertexes) {
      //     System.out.println(vertex.id + ":" + vertex.getInfo() + ":" + vertex.previous + "\n");
      //  }

   }


   /** Vertex represents node in a graph. Vertex has multiple arcs.
    * Info is used in dijkstra algorithm as path from root.
    * Previous vertex is the vertex that is closest to it.
    */
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      private Vertex previous;

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      private void setInfo (int infoToSet) {
         this.info = infoToSet;
      }

      public int getInfo () {
         return this.info;
      }

      public Vertex getPreviousVertex () { return this.previous; }

      private void setPrevious (Vertex previous) { this.previous = previous; }

   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info;

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      public int getArcInfo() { return this.info; }

      private void setArcInfo(int info) { this.info = info; }
   } 


   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      private ArrayList<Vertex> ListOfVertexes = new ArrayList<>();

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         ListOfVertexes.add(res);
         return res;
      }

      public ArrayList<Vertex> getVertexList () {
         return ListOfVertexes;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      /**
       * Set random arc lengths for every arc in graph.
       * @param highestInfo highest int that can occur
       */
      public void setRandomInfoForArc (int highestInfo){

         Random random = new Random();

         Vertex v = first;

         while (v != null){
            Arc a = v.first;
            while (a != null) {

               a.setArcInfo(random.nextInt(highestInfo - 1) + 1);

               a = a.next;
            }

            v = v.next;
         }
      }

      /** Dijkstra algorithm realization for given graph.
       * @param g Graph to augment
       */
      public void dijkstraShortestPathAlgorithm(Graph g){


         // Big enough value for
         int INFINITY = Integer.MAX_VALUE / 4;

         ArrayList<Vertex> visited = new ArrayList<>();
         ArrayList<Vertex> unVisited = new ArrayList<>(g.ListOfVertexes);

         // Set every vertex length from start to "infinity"
         for (Vertex vertex : unVisited) {
            if (vertex.equals(first)) {
               vertex.setInfo(0);
            } else {
               vertex.setInfo(INFINITY);
            }
         }

         // Take the last element from list of vertexes. Vertexes are contained in a list in the graph class.
         while (!unVisited.isEmpty()){
            Vertex vertexToVisitWithLowestInfo = unVisited.get(0);

            // In the first iteration. Every object info apart from the last object in list is "infinity".
            // Naturally first element is chosen to do the iteration
            for (Vertex vertex : unVisited) {
               if (vertex.getInfo() < vertexToVisitWithLowestInfo.info){
                  vertexToVisitWithLowestInfo = vertex;
               }
            }

            // Remove element from unvisited list since it is now visited.
            unVisited.remove(vertexToVisitWithLowestInfo);

            Arc a = vertexToVisitWithLowestInfo.first;

            // Iterate over every arc that originates from the vertex.
            // If a shorter path is found. Add this vertex to previous on the arc target.
            while (a != null){

               if (a.target.info >  a.info){
                  a.target.setInfo(a.info);
                  a.target.setPrevious(vertexToVisitWithLowestInfo);
               }

               a = a.next;
            }

            visited.add(vertexToVisitWithLowestInfo);
         }
      }

      /**
       * Find shortest cycle involing vertex.
       *
       * Shortest cycle is found by using dijkstra shortest path algorithm on graph and then looking at vertexes
       * that are attached to the vertex we want to determine the cycles from.
       *
       * @param v Vertex to determine shortest path from.
       * @param g Graph that the vertex originates from
       * @return
       */
      public ArrayList<Vertex> findShortestCycleInvolvingVertex(Vertex v, Graph g){

         if (v == null) {
            throw new RuntimeException("Vertex to determine cycles from is null in finding shortest cycle");
         }

         if (g == null) {
            throw new RuntimeException("Graph is null in finding shortest cycle");
         }

         if (g.first == null) {
            throw new RuntimeException("Empty graph is provided to finding shortest cycle");
         }

         if (!g.getVertexList().contains(v)) {
            throw new RuntimeException(
                    "Vertex to find cycles from does not exist in graph");
         }

         int maxIterations = g.ListOfVertexes.size();

         //Run algorithm to determine paths.
         dijkstraShortestPathAlgorithm(g);

         ArrayList<ArrayList<Vertex>> allCycles = new ArrayList<>();
         ArrayList<Vertex> shortestCycle = new ArrayList<>();

         // Possible solutions are all arcs going out form v.
         Stack<Arc> stackOfPossibleSolutions = new Stack<>();

         Arc a = v.first;

         // Push possible solutions to stack.
         while(a != null) {
            stackOfPossibleSolutions.push(a);
            a = a.next;
         }

         // Process stack.
         while(!stackOfPossibleSolutions.empty()){
            ArrayList<Vertex> cycle = new ArrayList<>();
            Vertex vertexToLookForCycles = stackOfPossibleSolutions.pop().target;
            int iterations = 0;

            // Shortest path can not be longer than vertexes combined
            while (!vertexToLookForCycles.equals(v) && iterations <= maxIterations){
               iterations += 1;
               cycle.add(vertexToLookForCycles);
               // Shortest path originates from previous vertexes
               vertexToLookForCycles = vertexToLookForCycles.getPreviousVertex();
            }
            allCycles.add(cycle);
         }

         int minDistance = Integer.MAX_VALUE / 4;

         // determine which cycle has the lowest length.
         for (ArrayList<Vertex> arrayList :
                 allCycles) {
            int minDistanceInArray = 0;
            for (Vertex vertex :
                    arrayList) {
               minDistanceInArray += vertex.info;
            }

            // Determine winner of cycles. If cycle lengths are even. Choose at random.
            if (minDistanceInArray < minDistance){
               shortestCycle.clear();
               shortestCycle.addAll(arrayList);
               minDistance = minDistanceInArray;
            }
         }

         return shortestCycle;
      }
   }
} 

